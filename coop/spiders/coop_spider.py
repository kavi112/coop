from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy.http import Request
from coop.items import CoopItem
# from mehtam.items import MathemItem

class Mehtem_spider(BaseSpider):
    name = "coop"
    allowed_domains = ["cooponline.se"]
    start_urls = ["http://www.cooponline.se/Matkasse/Bestall/430197.html"]

    def __init__(self):
        self.categories=[]
        self.productCategoryLinkList=[]
        # self.productID=[]
        # self.productName=[]
        # self.productImageUrl=[]
        # self.manufacturer=[]
        # self.price=[]
        # self.productDesc=[]
	    

    def parse(self, response):
        hxs = HtmlXPathSelector(response)
        self.categories=hxs.select('//*[@id="mainCategoriesMenu"]/ul/li/a/@href').extract()
        for i in range(len(self.categories)):
            yield Request(url="http://www.cooponline.se"+self.categories[i], callback=self.cat)
            
       
        print ("final"+str(len(self.productCategoryLinkList)))
        return	
    
    def cat(self,response):
        # print response.url
        
        
        hxs = HtmlXPathSelector(response)
        self.productCategoryLinkList+= hxs.select('//div[@class="productCategoryLinkList"]/a/@href').extract()
        # print ("loop:"+str(len(self.productCategoryLinkList)))
        if ((len(self.productCategoryLinkList))>545):
            for i in range(len(self.productCategoryLinkList)):
                yield Request(url="http://www.cooponline.se"+self.productCategoryLinkList[i], callback=self.cat1)

        return

    def cat1(self,response):
        items=[]
        hxs = HtmlXPathSelector(response)
        print response.url
        for i in range(len(hxs.select('//div[@class="ProductContainer"]/@productid').extract())):
            item=CoopItem()
            item['productID']=hxs.select('//div[@class="ProductContainer"]/@productid').extract()[i]
            item['productName']=hxs.select('//span[@class="groceryName"]/text()').extract()[i].strip()
            item['productImageUrl']=hxs.select('//div[@class="groceryImage"]/a/img/@src').extract()[i]
            item['productDesc']=hxs.select('//div[@class="readmoreText descriptionShort"]/text()').extract()[i].strip()
            item['manufacturer']=hxs.select('//span[@class="manufacturer"]/text()').extract()[i].strip()[1:]
            item['price']=hxs.select('//div[@class="normalprice"]/text()').extract()[i].strip()[6:]
            item['categoryPath']='-->'.join(hxs.select('//div[@class="breadCrumb"]/a/text()').extract())
            items.append(item)


        
        # self.productID+=hxs.select('//div[@class="ProductContainer"]/@productid').extract()
        # self.productName+= [x.strip() for x in hxs.select('//span[@class="groceryName"]/text()').extract()]
        # self.productImageUrl+= hxs.select('//div[@class="groceryImage"]/a/img/@src').extract()
        # self.productDesc+= [x.strip() for x in hxs.select('//div[@class="readmoreText descriptionShort"]/text()').extract()]
        # self.manufacturer+=[x.strip() for x in hxs.select('//span[@class="manufacturer"]/text()').extract()]
        # self.price+= [x.strip()[6:] for x in hxs.select('//div[@class="normalprice"]/text()').extract()]
        

        return items

# Scrapy settings for coop project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'coop'

SPIDER_MODULES = ['coop.spiders']
NEWSPIDER_MODULE = 'coop.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'coop (+http://www.yourdomain.com)'

ITEM_PIPELINES = [
  'scrapy_mongodb.MongoDBPipeline',
]
MONGODB_DATABASE = 'af_kavi112-kavi112'
# # mongodb://user:pass@host:port
MONGODB_URI='mongodb://kavi112:letmein@ds043348.mongolab.com:43348/af_kavi112-kavi112'
# # 'mongodb://8c7cfbf6-a08c-4913-98a2-a19498565f31:afd536b1-5454-4fde-8053-a180b25793ae@10.0.55.70:25001/db'
# # MONGODB_URI='mongodb://localhost:27017/db'
MONGODB_COLLECTION = 'coop_cart'
